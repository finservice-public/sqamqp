module Sqamqp
  class Config
    DEFAULTS = {
      user: 'guest',
      password: 'guest',
      host: '127.0.0.1',
      port: 5672,
      vhost: '/',
      pool: 10,
      network_recovery_interval: 5,
      heartbeat_interval: 60,
      recovery_completed: nil
    }.freeze

    attr_accessor :log_file, :log_level, :user, :host, :password, :port, :vhost, :pool, :network_recovery_interval,
                  :heartbeat_interval, :recovery_completed

    def options
      res = {}
      res[:log_file] = log_file if log_file
      res[:log_level] = log_level if log_level
      res
    end

    DEFAULTS.each do |setting, default_value|
      define_method setting do
        instance_variable_get("@#{setting}") || default_value
      end
    end
  end
end
